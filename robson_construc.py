#objeto cargo
class cargo:
    salario = 0
    #construtor
    def __init__(self,salario):
        self.salario = salario

#objeto funcionario
class funcionario:
    codigo = 0
    nome = ""
    codigo_cargo = 0
    #construtor
    def __init__(self,codigo,nome,codigo_cargo):
        self.codigo = codigo
        self.nome = nome
        self.codigo_cargo = codigo_cargo

class construc:
    cargos_list = []
    funcionarios_list = []

        #construtor
    def __init__(self):
        self.cargos_list = []
        self.funcionarios_list = []

    def addCargo(self,sal):
        self.cargos_list.append(cargo(sal))
    
    def addFunc(self,func):
        #verifica se o cargo eh valido
        if func.codigo_cargo < 0 or func.codigo_cargo > len(self.cargos_list)-1:
            return
        #verifica se o codigo do funcionario ja esta sendo usado
        for f in self.funcionarios_list:
            if f.codigo == func.codigo:
                return
        #insere no vetor de funcionario
        self.funcionarios_list.append(func)

    #print
    def relatorioFunc(self):
        print("codigo \t nome \t\t\t salario")
        print("------------------------------------------")
        #percorre a lista de funcionarios
        for fu in self.funcionarios_list:
            #juncao do vetor funcionarios com o vetor cargos
            print(f'{fu.codigo} \t {fu.nome} \t \t {self.cargos_list[fu.codigo_cargo].salario}')

    def relatorioCargo(self,id):
        #verifica se a entrada eh valida
        if id < 0 or id > len(self.cargos_list)-1:
            return

        #armazena o valor do salario do cargo a ser incrementado
        salario = self.cargos_list[id].salario

        #contador
        counter = 0

        #percorre o vetor de funcionarios
        for f in self.funcionarios_list:
            #caso a entrada e o cargo do funcionario sejam iguais o contador eh incrementado com o valor do cargo
            if f.codigo_cargo == id:
                counter += salario

        #print
        print(f'a soma dos salarios dos funcionario com cargo {id} eh: {counter}')

        return counter