#include <iostream>
#include <vector>

using namespace std;

//objeto funcionario
class funcionario{
    public:
        int codigo;
        string nome;
        int codigo_cargo;
        
    //construtor
    funcionario(int c,string n,int cg){
        codigo = c;
        nome = n;
        codigo_cargo = cg;
    }
};

//objeto cargo
class cargo{
    public:
        int salario;

    //construtor
    cargo(int s){
        salario = s;
    }
};

class db{
    public:

    //insere um cargo no vetor
    void cadastrar_cargo(cargo data){
        cargos.emplace_back(new cargo(data.salario));
    }

    //metodo booleano para redundancia
    bool cadastrar_funcionario(funcionario data){

        for(funcionario* f : funcionarios){
            //verifica se o codigo do funcionario ja esta sendo usado
            if(f->codigo == data.codigo){
                return false;
            }
        }

        //verifica se o cargo eh valido
        if(data.codigo_cargo > cargos.size()-1){
            return false;
        }

        //insere no vetor de funcionario
        funcionarios.emplace_back(new funcionario(data.codigo,data.nome,data.codigo_cargo));
        return true;
    }

    //print
    void relatorio_func(){
        cout << "codigo" << "\t" << "nome" << "\t" << "\t" <<"salario" << endl;
        cout << "---------------------------------" << endl;
        //percorre a lista de funcionarios
        for(funcionario* f : funcionarios){
            //juncao do vetor funcionarios com o vetor cargos
            cout << f->codigo << "\t" << f->nome << "\t" << "\t" << cargos.at(f->codigo_cargo)->salario << endl;
        }
    }


    double relatorio_carg(int pos){
        //verifica se a entrada eh valida
        if(pos < 0 || pos > cargos.size()-1){
            return -1;
        }
        //armazena o valor do salario do cargo a ser incrementado
        const double salario = cargos.at(pos)->salario;
        //contador
        double counter = 0;

        //percorre o vetor de funcionarios
        for(funcionario* f : funcionarios){
            //caso a entrada e o cargo do funcionario sejam iguais o contador eh incrementado com o valor do cargo
            if(f->codigo_cargo == pos){
                counter+=salario;
            }
        }
        //print
        cout << "a soma dos salarios dos funcionario com cargo " << pos << " eh: " << counter <<endl;
        return counter;
    }

    private:

    vector<funcionario*> funcionarios;
    vector<cargo*> cargos;
};