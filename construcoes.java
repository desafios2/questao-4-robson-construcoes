/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sources;

import java.util.ArrayList;
import javax.swing.JTextArea;
import sources.beans.cargo;
import sources.beans.funcionario;

/**
 *
 * @author matheus
 */
public class construcoes {
    ArrayList<cargo> cargos = new ArrayList<>();
    ArrayList<funcionario> funcionarios = new ArrayList<>();
    
    //insere um cargo no vetor
    public void addCargo(double salario){
        cargos.add(new cargo(salario));
    }
    
    //metodo booleano para redundancia
    public boolean addFuncionario(funcionario f){
        if(f.getCodigo_cargo() < 0 || f.getCodigo_cargo() > cargos.size() - 1){
            return false;
        }
        
        for (funcionario fun : funcionarios) {
            if(f.getCodigo() == fun.getCodigo()){
                return false;
            }
        }
        
        funcionarios.add(f);
        return true;
    }
    
    //print em jTextArea
    public void relatorioFuncionario(JTextArea out){
        String s = "Codigo \t Nome \t Salario:\n"
                + "-------------------------------\n";
        //percorre a lista de funcionarios
        for(funcionario f : funcionarios){
            //juncao do vetor funcionarios com o vetor cargos
            s += f.getCodigo()+"\t"+f.getNome()+"\t"+cargos.get(f.getCodigo_cargo()).getSalario()+"\n";
        }
        out.setText(s);
    }
    
    public void relatorioCargo(int cod,JTextArea out ){
        //verifica se a entrada eh valida
        if(cod < 0 || cod > cargos.size() - 1){
            return;
        }
        //armazena o valor do salario do cargo a ser incrementado
        double sal = cargos.get(cod).getSalario();
        //contador
        double counter = 0;
        
        //percorre o vetor de funcionarios
        for(funcionario f : funcionarios){
            if(f.getCodigo_cargo() == cod){
                counter += sal;
            }
        }
        
        //print em jTextArea
        out.setText("A soma dos salarios do cargo " + cod +" eh: "+ counter );
    }
    
    public int getCargosSize(){
        return cargos.size();
    }
    
    public int getFuncionariosSize(){
        return funcionarios.size();
    }
    
}
